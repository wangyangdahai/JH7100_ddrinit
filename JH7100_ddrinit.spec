Name:	 JH7100_ddrinit
Version: 211102
Release: 0
Summary: VisionFive ddrinit
URL:	 https://github.com/starfive-tech/JH7100_ddrinit
License: GPLv2

Source0: https://github.com/starfive-tech/JH7100_ddrinit/archive/refs/tags/JH7100_ddrinit-ddrinit-2133-%{version}.tar.gz

BuildRequires: gcc make

Patch0:  0000-rename.patch
Patch1:  0001-reduce-boot-wait-time.patch

%description
VisionFive ddrinit

%prep
%autosetup -n JH7100_ddrinit-ddrinit-2133-%{version} -p1

%build
pushd build
gcc -dumpspecs
export VERSION=%{version}
export CROSSCOMPILE=""
export LDFLAGS=""
make

%install
install -m 777 build/ddrinit-2133.bin $RPM_BUILD_ROOT
install -m 777 build/ddrinit-2133.elf $RPM_BUILD_ROOT

%files
/ddrinit-2133.bin
/ddrinit-2133.elf

%changelog
* Tue May 10 2022 wangyangdahai <admin@you2.yop> - 211102-0
- init
